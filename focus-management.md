# Focus management

## Question 1

### What is Deep Work?

- Focusing on a task that we are currently working on without any distractions of any kind for a specific period.

- Not switching the context from which we are working on.

- Stick to a task for a long time without any distractions of any kind.

## Question 2

### Paraphrase all the ideas in the above videos and this one in detail.

- In the first video, the author says, the optimal duration for deep work is 1 hour or sometimes even 90 min, so that we can get at least 40-45 min of deep focus on the task.

- In the second video, the author says, having a deadline reduces the frequency of taking a break and deadlines serve as a motivation to work, as one looks to finish the work before the deadline.

- The third video is about how deep work can help us to complete difficult tasks. The intense focus causes the myelin to develop in relevant areas of the brain, which allows the brain to fire faster and cleaner. In the current world, deep work is becoming valuable and real. The three deep work strategies are:

- Schedule your distraction

- Have deep work regularly

- Get valuable sleep

## Question 3

### How can you implement the principles in your day-to-day life?

- Practice deep work for at least one hour at a stretch, without switching context.

- Keep distractions aside.

- Don't look at smartphones or social media unnecessarily, which wastes a lot of time and reduces concentration.

- Make deep work a habit, and practice it every day.

- Having adequate sleep.

## Question 4

### Your key takeaways from the video

- Having social media is not that important to stay connected with people and friends.

- Social media are addictive and we think that they give us some entertainment on a busy day, which is not true.

- By using social media, we allow ourselves to be a source of income for companies by sharing our data.

- Avoiding social media can increase our productivity in professional work.

- Social media cause anxiety and increase stress levels which are harmful to our health.

- We can develop our skills better with no social media in our life by allowing us more time to work on skills that we need.

- A glance at social media can disturb our deep work.
