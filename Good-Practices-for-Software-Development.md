## Question 1

* Make sure to ask questions and seek clarity in the meeting itself. Since most of the times, it will be difficult to get the same set of people online again

* Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.

* Look at the way issues get reported in large open-source projects.

* Try to find out relevant team member's work schedules, and set up a call when they are free

* Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.

* We recommend tracking your time using app like Boosted to improve productivity.


## Question 2

### Which area do you think you need to improve on? 
* I sometimes get caught up in the process of building logic.

### What are your ideas to make progress in that area?
* I'll practise on my DSA and solve questions on a Leetcode-like platform on a daily basis.
