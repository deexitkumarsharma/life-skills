# Prevention of Sexual Harassment
 
### Q. What kinds of behavior cause sexual harassment?
 
Sexual harassment can happen in 3 ways that are unwelcome behavior in nature. These can affect the work environment very badly.
 
* Verbal
* Visual
* Physical
 
### Verbal Harassment:
* Comments about clothing
* Comments about a person's body
* Sexual or gender-based jokes or remarks
* Repeatedly asking out a person
* Spreading rumors
* Using abusive language
 
### Visual Harassment :
* Posters
* Drawing pictures
* Cartoons
* Emails
 
### Physical Harassment :
* Touching the person's body, clothing
* Hugging, kissing, patting, or stroking
* Touching or rubbing oneself
* Sexual gesturing
 
These can be categorized into two types:
1. Quid pro quo harassment: It refers to the expectation of sexual favors or advances in exchange for professional rewards such as promises of promotion, increased compensation, scholastic performance, and so on.
2. Hostile work environment: This happens when an employee experiences unwelcome physical or verbal behaviour of a sexual nature.
 
### Q. What would you do in case you face or witness any incident or repeated incidents of such behavior?
 
Immediately we can tell them to stop the harassment. If that does not happen, we can tell the supervisor and higher authority who can act on it and make sure harassing stops.
 
In addition to reporting, you can document everything. The following measures may be taken to document the situation:
 
1. Note these details:
The date, time and place of the harassment, what occurred, what was said and who witnessed the behaviour.

2. Keep copies or screenshots of any relevant email, text, photo or social posts.

3. Tell a trusted friend, family member or colleague about what happened and write down details of those conversations.
 
4. Keep all records outside of your office or work computer and ensure they are securely stored.
 
This can be reported to affected officials with appropriate evidence.
