# REST-Architecture
## What is called an API?
- **API** stands for **A**pplication **P**rogramming **I**nterface. 
- In general, an **API** is a set of programming code that enables data transmission between one software product and another.

![API](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQDzT7su1ZcjFDC24OQ3aEDv2Noh6_SZhsJug&usqp=CAU)

Click [here](https://en.wikipedia.org/wiki/API) for more about an **API**.


## What is REST?
- REST, or Representational State Transfer, is an architectural style for building web services. REST is a popular choice for building web services because it is     simple and scalable.
- In a RESTful system, data is represented in resources. A resource is a piece of data that can be identified by a URL. Each resource has its own URL, and resources can be nested inside of other resources.
- You can also create, update, or delete resources using the HTTP methods POST, PUT, and DELETE respectively. These methods are known as the "uniform interface" because they are the same for all resources.
- The REST sent the information in the form of JSON,XML,HTML, etc,.
- Following diagram explains better to you.

## REST Definition ?
- **REST** stands for **R**epresentational **S**tate **T**ransfer.
- **REST** is a software architectural style as well as an approach for communication purpose that is often used in various web services development.
- The goal of **REST** is to increase performance, scalability, simplicity, modifiability, visibility, portability, and reliability.
- These goals are achieved through following **REST** principles.

### REST Principles :
- **Client–server architecture**
  + The client-server constraint works to separate the user interface from the data storage. This means that the server should not store any data related to the client's session. Instead, the server should just provide resources that the client can access.
- **Statelessness**
  + The stateless constraint means that each request from a client must contain all of the information necessary for the server to understand and process it. The server should not store any information about previous requests from the same client.
- **Cacheability**
  + The cacheable constraint allows for responses from the server to be cached by intermediate proxies or clients. This can improve performance by reducing network traffic and latency.
- **Layered system**
  + Adding security as a separate layer enforces security policies.
- **Code on demand**
  + Servers can temporarily extend or customize the functionality of a client by transferring executable code.
- **Uniform interface**
  + The uniform interface constraint defines a set of standard components that must be used in order for two systems to communicate with each other. These components include identification of resources, manipulation of resources through representations, self-descriptive messages, and hypermedia as the engine of application state (HATEOAS).
  + The four constraints for this uniform interface are :
    * Resource identification in requests
    * Resource manipulation through representations
    * Self-descriptive messages
    * Hypermedia as the engine of application state (HATEOAS)

## HTTP Methods used by REST Architecture
The REST architecture makes use of five HTTP methods.

- **GET:** This method helps in offering read only access for the resources.
- **POST:** This method is implemented for creating a new resource.
- **DELETE:** This method is implemented for removing a resource.
- **PUT:** This method is implemented for updating an existing record in the data source.
- **PATCH:** The PATCH request only needs to contain the changes to the resource, not the complete resource.

## Advantages of REST
- There are many benefits to using REST in your application design. REST is designed to be simple and easy to use. It is based on the HTTP protocol, which is the most widely used protocol on the web. This makes it easy to integrate REST into existing applications and systems.
- REST also scalability. When designing a RESTful application, you can start small and scale up as needed. This makes it ideal for growing businesses or organizations with changing needs.
- REST is also designed to be extensible. You can add new features or functionality to a RESTful application without affecting the existing application structure. This makes it easy to evolve your application as users' needs change over time.

## REST API Security
- A RESTful API provides another route to access and manipulate your application. Even if it’s not an interesting hacking target, a badly behaved client could send thousands of requests every second and crash your server.

## CONCLUSION
- In conclusion, a restful architecture is vital to any system that needs to communicate over the internet. By understanding the six constraints of REST, you can create a more reliable and scalable system. Keep these concepts in mind as you design your next project, and you'll be on your way to success.

## REFERENCE
- [https://blog.axway.com/learning-center/apis/basics/what-is-an-api]
- [https://www.altexsoft.com/blog/rest-api-design/]
- [https://restfulapi.net/]
- [https://www.brcline.com/blog/what-are-rest-apis-constraints]
