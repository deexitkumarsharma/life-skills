# Grit and Growth Mindset

## 1. Grit

### Q1. Paraphrase (summarize) the video in a few lines. Use your own words.

- Those with tenacity can accomplish more in life than those with the most talent.
- Studies show that those with grit may endure despite challenging circumstances.
- Keeping our promises can help us develop grit.
- Talent is inversely correlated with grit.
- Grit can only be developed by adopting a growth attitude.

### Q2: What are your key takeaways from the video to take action on?

- Grit, not IQ, is what matters most to achieve in life.
- "Grit" is the desire to achieve a goal.
- Avoid falling when you fail. Instead, learn and grow.
- To keep trying after failing.
- One's performance is improved by having a growth mentality.

# 2: An Overview of the Growth Mindset

### Q3. Paraphrase (summarize) the video in a few lines in your own words.

- It is expertly described how having a growth mindset can help you lead a successful life. To improve your life, all you need to do is alter your perspective. 
- To adopt a growth mindset, one must make an effort to learn, accept difficulties and give them their best, accept failures and turn them into successes, and take feedback and work on it.

### Q4: What are your key takeaways from the video to take action on?

- Have faith in our ability to overcome obstacles and problems.
  Do not quit.
  Don't say that everyone who does anything is God-given.
- State that you believe you can figure it out today, take a break, and then return.
- Believe in yourself and your abilities; set goals for yourself.

## 3. Understanding Internal Locus of Control

### Q5. What is the internal locus of control? What is the key point in the video?

- The internal locus of control is what drives you to do the task; it is the ability to ask yourself, "Why am I not doing this?" Why can't I do this? And you roll up your sleeves and do the work. These are the factors that prompted you to disregard excuses and begin looking for reasons to complete the task. It is the state in which you are self-motivated.

- This video highlighted many important points to remember, such as the importance of remaining positive, being self-motivated, looking for reasons to do the task, and never looking for excuses. This video taught us to be doers rather than complainers and that the only thing standing between us and success is an emotional barrier.

## 4. How to build a Growth Mindset

### Q6. Paraphrase (summarize) the video in a few lines in your own words.

- A growth mentality is based on the notion that everything is possible.
- Do not assume that you cannot enhance your abilities. Instead, consider who you are now and what you can become.
- Believe that you can change; don't think that who you are today will be the same in the future.
- Create your own life's curriculum.

- Refrain from becoming discouraged when you fail, and instead embrace the challenges.

### Q7. What are your key takeaways from the video to take action on?

- Believe in yourself and your ability to achieve better results through hard work. Questioning ourselves about what we want to become and what we can do to get there can change our mindset. 

- Focusing on work can improve our results.
- Don't get discouraged when mistakes happen.
- Accept the challenges, even though they are tough.

## 5. Mindset: A Mount Blue Warrior Reference Manual

[Google Docs - Click Here](https://docs.google.com/document/d/1SPUqC-8WwfiDlsRGKWqoMtC14v6_2TEhq7LZs29bJWk/edit)

### Q8. What are one or more points that you want to take action on from the manual?

- Stick to a problem till I complete it.
- I will understand each concept properly.
- I will have confidence in myself and stand by it.
