# Listening and Active Communication

## 1. Active Listening

#### Q.1 What are the steps/strategies to do Active Listening?

- **Face the speaker and have eye contact:** Eye contact is an important part of face-to-face conversation. Too much eye contact can be intimidating, though, so adapt this to the situation you’re in.

- **Don’t interrupt:** Being interrupted is frustrating for the other person – it gives the impression that you think you’re more important or don’t have time for what they have to say.

- **Stay focused:** If you're finding it difficult to focus on what someone is saying, try repeating their words in your head as they say them.

- **Ask questions:** Asking relevant questions can show that you’ve been listening and help clarify what has been said.

- **Don’t start planning what to say next:** You can’t listen and prepare at the same time.

- **Don’t impose your opinions or solutions:** It’s not always easy, but lending a listening, supportive ear can be much more rewarding than telling someone what they should do.

## 2.Reflective Listening

#### Q.2 According to Fisher's model, what are the key points of Reflective Listening?

The key points of reflective listening according to Fisher's model are as follows:

- We must concentrate on what the other person is saying without interrupting.
- We have to encourage each other to speak freely. By embracing the speaker's perspective
- We must quiet our minds and focus completely on the speaker's mood, reflecting his emotional state through words and non-verbal communication.
- We can summarise what the speaker said, using his own words.
- We have to respond to the speaker's specific point without deviating from other subjects.

## 3.Reflection

#### Q.3 What are the obstacles in your listening process?

1. **Noise:** Any external noise can be a barrier, like the sound of equipment running, phones ringing, or other people having conversations.

2. **Visual distractions:** Visual distractions can be as simple as the scene outside a window or the goings-on just beyond the glass walls of a nearby office.

3. **Physical setting:** An uncomfortable temperature, poor or nonexistent seating, bad odors, or distance between the listener and speaker can be an issue.

4. **Anxiety:** Anxiety can take place from competing for personal worries and concerns.

5. **Self-centeredness:** This causes the listener to focus on his or her thoughts rather than the speaker’s words.

6. **Mental laziness:** Laziness creates an unwillingness to listen to complex or detailed information.

7. **Boredom:** Boredom stems from a lack of interest in the speaker’s subject matter.

8. **Impatience:** A listener can become impatient with a speaker who talks slowly or draws out the message.

#### Q.4 What can you do to improve your listening?

1. Consider eye contact.

2. Be alert, but not intense.

3. Pay attention to nonverbal signs, such as body language and tone.

4. Make a mental image of what the speaker is saying.

5. Provide feedback.

6. Keep an open mind.

## 4. Types of Communication

#### Q.5 When do you switch to a Passive communication style in your day-to-day life?

- Passive communication style can be switched when some of the points occurred:-

1. When people may disregard your needs and opinions.

2. When you’re likely to be passed over for projects and promotions and miss out on interesting opportunities.

3. Sometimes you become complicit in poor choices because you don’t express your discomfort.

4. When you may feel angry, resentful, or stressed by other people’s inconsideration.

5. When you may bottle up your feelings claiming to be a peace-maker. But, in reality, you feel powerless and hopeless.

#### Q.6 When do you switch to Aggressive communication styles in your day-to-day life?

- Aggressive communication can be switched when some of the points occurred:-

1. When I avoid coming to the point. I tend to beat around the bush and drop hints, hoping that others will understand.

2. When I speak softly and apologetically. I am not confident in my thoughts, opinions, and actions, and don’t want to be seen as harsh or opinionated.

3. When someone annoys me during my work and keeps poking me with some nonsense question or something.

#### Q.7 When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?

- Passive Aggressive communication can be switched when some of the points occur:-

1. Sarcasm.

2. Emotional withdrawal.

3. Talking behind someone’s back.

4. Quitting unexpectedly with no explanation.

#### Q.8 How can you make your communication assertive? You can analyze the videos and then think about what steps you can apply in your own life.

1.  **Believe in yourself:** Low self-esteem can prevent you from telling others what you want. So, believe in yourself.

2.  **Start small:** If you’re having a hard time finding that assertive voice, begin with small things.

3.  **Be simple and direct:** Don’t leave room for misinterpretation. Assertiveness is effective because it is straight to the point.

4.  **Keep it positive:** One way to stop procrastinating and deal with situations requiring you to be assertive is to approach them positively.

5.  **Learn how to say “no”:** If you have a lot on your plate already and you can’t take on more at the moment, simply say no.
