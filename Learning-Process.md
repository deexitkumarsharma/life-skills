## Question 1
What is the Feynman Technique? Paraphrase the video in your own words.
 
## Answer 1
 
The Feynman Technique is a method of learning that takes something difficult to recognize and tries to make it clear in your mind by explaining it as if you were talking to a child.<br />
 <br />Basically, The Feynman method of teaching and communicating uses a mental model to communicate ideas clearly and simply.
 
 
## Question 2
 
What are the different ways to implement this technique in your learning process?
 
 ## Answer 2
 
* Make a clear plan for what you want to do and write it down on a clean piece of paper. Be more efficient, patient, and specific to our goal.

* Review our notes to make sure you didn't mistakenly borrow anything complicated. If there's something confusing, that's a good indication that you need to reflect on it and refine it.

* We can analyse the concept and try to teach someone or something about it, identifying gaps in your knowledge. 

 
## Question 3
 
Paraphrase the video in detail in your own words.
 
## Answer 3

If you find yourself concentrating on something, such as trying to learn a new concept or solve a problem, and you become stuck, you should shift your focus away from the problem and allow the diffuse modes to take over. Those in resting states do their work in the background.


## Question 4
What are some of the steps that you can take to improve your learning process?
 
## Answer 4

1. Relaxation is an important part of the learning process.
2. Within a matter of a few days, exercise can increase our ability to both learn and remember.
3. Tests are the best. Test yourself all the time. Give yourself little mini-tests.
4. The most effective technique is simply to look at a page, look away, and see what you can recall. Doing this, as it seems, helps build profound neural hooks that enhance your understanding of the material. The understanding alone is enough to build mastery of the material.
5. Practice and repetition in a variety of circumstances can help you truly gain mastery over what you're learning.
 
## Question 5
 
Your key takeaways from the video? Paraphrase your understanding.
 
## Answer 5
 
The biggest problems to all learners are procrastination and emotional fear. The learning technique in this video is a solution to those problems. Instead of targeting to be perfect and learning a bunch of skills at a time, break the big skill down into elementary pieces, pick the most essential skills and practice for at least 20 hours. The principle is simple, straight and easy to apply.

In addition, he recommends some ways to practice more effectively like:
- Defining mastery levels you want.
- eliminate barriers, especially emotional barriers.

According to this major barrier to learning new things, it's not intellectual, it's emotional! We all start at zero, but if we keep practising and working hard on it, we will soon get better at it. Enjoy what you're doing and find a way that works best for us. So, it will achieve our happiness and our goals. 
 
## Question 6
 
## What are some of the steps that you can while approaching a new topic?
 
## Answer 6
 
1: Decide exactly what you want to be able to do, and then look at the skill and break it down into small pieces. Most of the skills we want to learn are actually big skill packages that require different things. So, that way, we can practise the most important things first, and that way, we will be able to improve our performance in the shortest possible time.<br />
<br />2: The second is to learn enough to self-correct. So we should pick three to five resources about what we're trying to learn. We shouldn't use this as a way to postpone the practice. Practice should start right away, and what we want is to learn enough to be able to self-correct or self-edit as we practice. So learning becomes a way to improve and notice when we are making mistakes, and that way we can do something different.<br />
<br />3: Remove barriers to practice. Distractions, television, the internet, and everything else that gets in the way of us sitting down to study or work.In this way, the more we are able to use some of our willpower to remove the distractions that are keeping us from practicing, the more we are able to sit down and practice.<br />
<br />4: Practice for at least 20 hours.
 

